<?php
include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
class Shortcodes_Master_Wcfpc_Shortcodes
    {
        
    function __construct() { }

/**
 * 
 * wcfpc
 */
    public static function wcfpc($atts = null, $content = null)
        {
        	if(!is_plugin_active('woocommerce/woocommerce.php')) return;
        $atts=shortcode_atts(array
            (
            'id' => 'wcfpc-unique-id',
            'autoplay' => 'yes',
            'items' => '3',
            'bg_color' => '#ffffff',
            'title_color' => '#777777',
            'regularprice_color' => '#27AE60',
            'saleprice_color' => '#C0392B',            
            'badge_color' => '#E76453',
            'badgetext_color' => '#FFFFFF',
            'navigation' => 'yes'
            ), $atts, 'wcfpc');

        sm_query_asset( 'css', 'font-awesome' );
        sm_query_asset( 'css', 'owl-carousel' );
        sm_query_asset( 'css', 'wcfpc-theme-default' );
        sm_query_asset( 'css', 'owl-transitions' );
        sm_query_asset( 'css', 'wcfpc' );
        sm_query_asset( 'js','jquery' );
        sm_query_asset( 'js','owl-carousel' );
        sm_query_asset( 'js','wcfpc' );

        $autoplay = ($atts['autoplay'] === "yes" ? "true" : "false");
        $navigation = ($atts['navigation'] === "yes" ? "true" : "false");      
    
global $wpdb, $product;
$my_attachment_objects = array();

$args = array(
    'post_type' => 'product',
    'meta_key' => '_featured',
    'meta_value' => 'yes',
    'posts_per_page' => -1
);

$featured_query = new WP_Query( $args );
    
    $return = '<div id="'.$atts['id'].'" class="owl-carousel wcfpc-theme-default">';
    if ($featured_query->have_posts()) : 

    while ($featured_query->have_posts()) : 
    
        $featured_query->the_post();
        
        $product = get_product( $featured_query->post->ID );
        
        // Output product information here
        /*echo "<pre>";
        print_r($product);
        echo "</pre>";*/        

        // arguments for get_posts
        global $post;
    $attachment_args = array(
        'post_type' => 'attachment',
        'post_mime_type' => 'image',
        'post_status' => null, // attachments don't have statuses
        'post_parent' => $post->ID
    );
    // get the posts
    $this_posts_attachments = get_posts( $attachment_args );
    // append those posts onto the array
    $my_attachment_objects[$post->ID] = $this_posts_attachments;

    $sale_badge = get_post_meta( get_the_ID(), '_sale_price') !='' ? '<div class="wcfpc-onsale" style="background-color: '.$atts['badge_color'].'; color: '.$atts['badgetext_color'].';">Sale</div>' : '';

    if(get_post_meta( get_the_ID(), '_sale_price')) { $sale_price = '<span class="wcfpc-price-sale" style="color: '.$atts['saleprice_color'].';">'.get_woocommerce_currency_symbol().''.get_post_meta( get_the_ID(), '_sale_price').'</span>'; } else { $sale_price = ''; }
        $return .= '<div class="item" style="background-color: '.$atts['bg_color'].';">
            <div class="wcfpc-image">

            <div class="wcfpc-wrap-image">
            
            <img src="'.wp_get_attachment_url( get_post_thumbnail_id($post->ID)).'">
            <div class="clear"></div>
            </div><div class="clear"></div>
            </div>

            <div class="wcfpc-details">
            <h2 class="wcfpc-title" style="color: '.$atts['title_color'].';">'.get_the_title().'</h2>
            <div class="wcfpc-price">
            '.$sale_price.'
            <span class="wcfpc-price-regular" style="color: '.$atts['regularprice_color'].';">'.get_woocommerce_currency_symbol().''.get_post_meta( get_the_ID(), '_regular_price').'</span>                
            </div>
            
            </div>
            '.$sale_badge.'
          <a class="wcfpc-permalink" href="'.get_permalink().'">More info</a>

        </div>';

        endwhile;
    
endif;

wp_reset_query(); // Remember to reset

        $return .= '</div>';
        $return .= '<script  type="text/javascript">jQuery(document).ready(function() {
            var owl = jQuery("#'.$atts['id'].'"); owl.owlCarousel({
                baseClass : "owl-carousel",
                theme : "wcfpc-theme-default",
                navigation : '.$navigation.',
                pagination: false, 
                navigationText: true,
                navigationText : ["<i class=\'fa fa-chevron-left\'><i>","<i class=\'fa fa-chevron-right\'><i>"], 
                items : '.$atts['items'].', //10 items above 1000px browser width
                itemsDesktop : [1000,'.$atts['items'].'], //5 items between 1000px and 901px
                itemsDesktopSmall : [900,'.$atts['items'].'], // betweem 900px and 601px
                itemsTablet: [600,'.$atts['items'].'], //2 items between 600 and 0
                itemsMobile : false, // itemsMobile disabled - inherit from itemsTablet option
                autoHeight: true,
                responsive: true,
                responsiveRefreshRate : 200,
                responsiveBaseWidth: window,
                autoPlay : '.$autoplay.'
        }); });</script>';
        return $return;
        }
        
    }
