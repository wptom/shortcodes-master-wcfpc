<?php
class Shortcodes_Master_Wcfpc {

	/**
	 * Constructor
	 */
	function __construct() {
		// Load textdomain
		load_plugin_textdomain( 'smwcfpc', false, dirname( plugin_basename( SMWCFPC_PLUGIN_FILE ) ) . '/lang/' );
		// Reset cache on activation
		if ( class_exists( 'Sm_Generator' ) ) register_activation_hook( SMWCFPC_PLUGIN_FILE, array( 'Sm_Generator', 'reset' ) );
		// Init plugin
		add_action( 'plugins_loaded', array( __CLASS__, 'init' ), 20 );
		// Make pluin meta translation-ready
		__( 'ShortcodesMaster', 'smwcfpc' );
		__( 'Shortcodes Master - Woocommerce featured products carousel', 'smwcfpc' );
		__( 'Woocommerce featured products carousel addon for Shortcodes Master.', 'smwcfpc' );
	}

	/**
	 * Plugin init
	 */
	public static function init() {
		// Check for SM
		if ( !function_exists( 'shortcodes_master' ) ) {
			// Show notice
			add_action( 'admin_notices', array( __CLASS__, 'sm_notice' ) );
			// Break init
			return;
		}
		// Register assets
		add_action( 'init', array( __CLASS__, 'assets' ) );
		// Add new group to Generator
		add_filter( 'sm/data/groups', array( __CLASS__, 'group' ) );
		// Register new shortcodes
		add_filter( 'sm/data/shortcodes', array( __CLASS__, 'data' ) );
		// Add plugin meta links
		add_filter( 'plugin_row_meta', array( __CLASS__, 'meta_links' ), 10, 2 );
	}

	/**
	 * Install SM notice
	 */
	public static function sm_notice() {
?><div class="updated">
			<p><?php _e( 'Please install and activate latest version of <b>Shortcodes Master</b> to use it\'s addon <b>Shortcodes Master - Woocommerce featured products
carousel</b>.<br />Deactivate this addon to hide this message.', 'smwcfpc' ); ?></p>
			<p><a href="https://lizatom.com/wordpress-shortcodes-plugin/" target="_blank" class="button button-primary"><?php _e( 'Install Shortcodes Master', 'smwcfpc' ); ?> &rarr;</a></p>	
			</div><?php
	}

	public static function assets() { 
        $plugins_url = plugins_url();

		wp_register_style( 'owl-carousel', plugins_url( 'assets/owl-carousel/owl.carousel.css',SMWCFPC_PLUGIN_FILE ), false, 
SMWCFPC_PLUGIN_VERSION, 'all' );
        wp_register_style( 'wcfpc-theme-default', plugins_url( 'assets/owl-carousel/wcfpc.theme.default.css', 
SMWCFPC_PLUGIN_FILE ), false, SMWCFPC_PLUGIN_VERSION, 'all' );
        wp_register_style( 'owl-transitions', plugins_url( 'assets/owl-carousel/owl.transitions.css', SMWCFPC_PLUGIN_FILE ), false, 
SMWCFPC_PLUGIN_VERSION, 'all' );
        wp_register_style( 'wcfpc', plugins_url( 'assets/css/wcfpc.css', SMWCFPC_PLUGIN_FILE ), false, SMWCFPC_PLUGIN_VERSION, 'all' );
        
        wp_register_script('jquery', includes_url('js/jquery/jquery.js'));       
        wp_register_script( 'owl-carousel', plugins_url( 'assets/owl-carousel/owl.carousel.min.js', SMWCFPC_PLUGIN_FILE ), array('jquery'), SMWCFPC_PLUGIN_VERSION, true );
        wp_register_script( 'wcfpc', plugins_url( 'assets/js/wcfpc.js', SMWCFPC_PLUGIN_FILE ), array('jquery'), 
SMWCFPC_PLUGIN_VERSION, true );
        
	}

	public static function meta_links( $links, $file ) {
		if ( $file === plugin_basename( SMWCFPC_PLUGIN_FILE ) ) $links[] = '<a href="https://lizatom.com/wiki/shortcodes-master-woocommerce-carousel/" target="_blank">' . __( 'Help', 'smwcfpc' ) . '</a>';
		return $links;
	}

	/**
	 * Add new group to the Generator
	 */
	public static function group( $groups ) {
		$groups['woocommerce'] = __( 'Woocommerce', 'smwcfpc' );
		return $groups;
	}

	/**
	 * New shortcodes data
	 */
	public static function data( $shortcodes ) {
        
        /*wcfpcs*/
        
		$shortcodes['wcfpc'] = array(
			'name'     => __( 'Woocommerce FP carousel', 'smwcfpc' ),
			'type'     => 'single',
			'group'    => 'woocommerce',
            'content' =>  __( "[%prefix_wcfpc id=\"unique-wcfpc-id\"]", 'smwcfpc' ),             
			'desc'     => __( 'Woocommerce featured products carousel.', 'smwcfpc' ),
			'note'     => sprintf( __( 'Read more on this shortcode %s here %s', 'sm' ), '<a href="https://lizatom.com/wiki/shortcodes-master-woocommerce-carousel/" target="_blank">', '&rarr;</a>' ),
			'icon'     => 'picture-o',
			'function' => array( 'Shortcodes_Master_Wcfpc_Shortcodes', 'wcfpc' ),
	            'atts' => array(
                        'id' => array(
                            'default' => 'unique-wcfpc-id',
                            'name' => __( 'ID', 'smwcfpc' ),
                            'desc' => __( 'Required! Insert unique id of the slider. Example: unique-wcfpc-id', 'smwcfpc')),
                        'autoplay' => array(
                                        'type' => 'bool',
                                        'default' => 'yes',
                                        'name' => __( 'Autoplay', 'smm' ),
                                        'desc' => __( 'Run the slider automatically.', 'smwcfpc' )
                        ),
                        'items' => array(
                            'type' => 'slider',
                            'min' => 1,
                            'max' => 20,
                            'step' => 1,
                            'default' => 3,
                            'name' => __( 'Items', 'smwcfpc' ),
                            'desc' => __( 'Number of items to be displayed.', 'smwcfpc' )
                        ),
                        'bg_color' => array(
                                        'type'    => 'color',
                                        'default' => '#ffffff',
                                        'name'    => __( 'Background color #1', 'smwcfpc' ),
                                        'desc'    => __( 'Background color #1.', 'smwcfpc' )
                        ),   
                        'title_color' => array(
                                        'type'    => 'color',
                                        'default' => '#777777',
                                        'name'    => __( 'Title color', 'smwcfpc' ),
                                        'desc'    => __( 'Color of the title.', 'smwcfpc' )
                        ),
                        'regularprice_color' => array(
                                        'type'    => 'color',
                                        'default' => '#27AE60',
                                        'name'    => __( 'Regular price color', 'smwcfpc' ),
                                        'desc'    => __( 'Color of the regular price.', 'smwcfpc' )
                        ),
                        'saleprice_color' => array(
                                        'type'    => 'color',
                                        'default' => '#C0392B',
                                        'name'    => __( 'Sale price color', 'smwcfpc' ),
                                        'desc'    => __( 'Color of the sale price.', 'smwcfpc' )
                        ),
                        'badge_color' => array(
                                        'type'    => 'color',
                                        'default' => '#E76453',
                                        'name'    => __( 'Badge background color', 'smwcfpc' ),
                                        'desc'    => __( 'Color of the badge background.', 'smwcfpc' )
                        ),
                        'badgetext_color' => array(
                                        'type'    => 'color',
                                        'default' => '#FFFFFF',
                                        'name'    => __( 'Badge text color', 'smwcfpc' ),
                                        'desc'    => __( 'Color of the badge text background.', 'smwcfpc' )
                        ),
                        'navigation' => array(
                                        'type' => 'bool',
                                        'default' => 'yes',
                                        'name' => __( 'Navigation', 'smwcfpc' ),
                                        'desc' => __( 'Display arrow navigation?', 'smwcfpc' )
                        ),            
                    )
		);
                 
		return $shortcodes;
	}
}

new Shortcodes_Master_Wcfpc;
