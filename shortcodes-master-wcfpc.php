<?php
/*
  Plugin Name: Shortcodes Master - Woocommerce featured products carousel
  Plugin URI: https://lizatom.com/wordpress-woocommerce-carousel/
  Version: 1.0.0
  Author: Lizatom.com
  Author URI: https://lizatom.com
  Description: Woocommerce featured products carousel with skins.
  Text Domain: smwcfpc
  Domain Path: /lang
  License: license.txt
 */

define( 'SMWCFPC_PLUGIN_FILE', __FILE__ );
define( 'SMWCFPC_PLUGIN_VERSION', '1.0.0' );

require_once 'inc/wcfpc.php';
require_once 'inc/shortcodes.php';
